package com.topresult.app.service;

import com.topresult.app.exception.UserLevelAlreadyExistsException;
import com.topresult.app.model.Model;

import java.util.List;

public interface StorageService {

    void add(Model model) throws UserLevelAlreadyExistsException;
    List<Model> getByUserId(Long userId);
    List<Model> getByLevelId(Long levelId);

}
