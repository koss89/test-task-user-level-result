package com.topresult.app.service;

import com.topresult.app.exception.UserLevelAlreadyExistsException;
import com.topresult.app.model.Model;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class StorageServiceImpl implements StorageService {

    @Autowired
    Environment env;
    private List<Model> collection = new ArrayList<>();

    public void add(Model model) throws UserLevelAlreadyExistsException {
        if(isUserLevelExists(model.getUserId(), model.getLevelId())) {
            throw new UserLevelAlreadyExistsException("User already have level:" + model.getLevelId());
        }
        collection.add(0, model);
        limitationStorage(model.getUserId());
    }

    private void limitationStorage(Long userId) {
        Long limit = env.getProperty("app.storage.limit", Long.class, 20L);
        List<Model> collect = collection.stream()
                .filter(item -> item.getUserId().equals(userId))
                .skip(limit)
                .collect(Collectors.toList());
        collection.removeAll(collect);
    }

    private boolean isUserLevelExists(Long userId, Long levelId) {
        return collection.stream()
                .anyMatch(item -> item.getUserId().equals(userId) && item.getLevelId().equals(levelId));
    }

    public List<Model> getByUserId(Long userId) {
        Long resultLimit = env.getProperty("app.result.limit", Long.class, 20L);
        return collection.stream()
                .filter(item -> item.getUserId().equals(userId))
                .sorted((firstItem, secondItem) -> Long.compare(secondItem.getResult(), firstItem.getResult()))
                .limit(resultLimit)
                .collect(Collectors.toList());
    }

    public List<Model> getByLevelId(Long levelId) {
        Long resultLimit = env.getProperty("app.result.limit", Long.class, 20L);
        return collection.stream()
                .filter(item -> item.getLevelId().equals(levelId))
                .sorted((firstItem, secondItem) -> Long.compare(secondItem.getResult(), firstItem.getResult()))
                .limit(resultLimit)
                .collect(Collectors.toList());
    }
}
