package com.topresult.app.controller;

import com.topresult.app.dto.LevelInfoDTO;
import com.topresult.app.dto.UserInfoDTO;
import com.topresult.app.model.Model;
import com.topresult.app.service.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class InfoController {

    @Autowired
    StorageService storageService;

    @GetMapping("userinfo/{userId}")
    public List<UserInfoDTO> getByUser(@PathVariable Long userId) {
        return  storageService.getByUserId(userId).stream()
                .map(UserInfoDTO::new)
                .collect(Collectors.toList());
    }

    @GetMapping("levelinfo/{levelId}")
    public List<LevelInfoDTO> getByLevel(@PathVariable Long levelId) {
        return  storageService.getByLevelId(levelId).stream()
                .map(LevelInfoDTO::new)
                .collect(Collectors.toList());
    }

    @PutMapping("setinfo")
    public void addInfo(@Valid @RequestBody Model model) throws Exception {
        storageService.add(model);
    }

}
