package com.topresult.app.dto;

import com.topresult.app.model.Model;

public class LevelInfoDTO {
    private Long user_id;
    private Long result;

    public LevelInfoDTO(Model model) {
        setUserId(model.getUserId());
        setResult(model.getResult());
    }

    public Long getUserId() {
        return user_id;
    }

    public void setUserId(Long user_id) {
        this.user_id = user_id;
    }

    public Long getResult() {
        return result;
    }

    public void setResult(Long result) {
        this.result = result;
    }
}
