package com.topresult.app.dto;

import com.topresult.app.model.Model;

public class UserInfoDTO {

    private Long level_id;
    private Long result;

    public UserInfoDTO(Model model) {
        setLevelId(model.getLevelId());
        setResult(model.getResult());
    }

    public Long getLevelId() {
        return level_id;
    }

    public void setLevelId(Long level_id) {
        this.level_id = level_id;
    }

    public Long getResult() {
        return result;
    }

    public void setResult(Long result) {
        this.result = result;
    }
}
