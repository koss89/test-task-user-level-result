package com.topresult.app.model;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

public class Model implements Serializable {

    @NotNull
    private Long user_id;
    @NotNull
    private Long level_id;
    @NotNull
    private Long result;

    public Model(Long user_id, Long level_id, Long result) {
        this.user_id = user_id;
        this.level_id = level_id;
        this.result = result;
    }

    public Long getUserId() {
        return user_id;
    }

    public void setUserId(Long user_id) {
        this.user_id = user_id;
    }

    public Long getLevelId() {
        return level_id;
    }

    public void setLevelId(Long level_id) {
        this.level_id = level_id;
    }

    public Long getResult() {
        return result;
    }

    public void setResult(Long result) {
        this.result = result;
    }
}
