package com.topresult.app.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT)
public class UserLevelAlreadyExistsException extends RuntimeException {

    public UserLevelAlreadyExistsException(String message) {
        super(message);
    }

}
