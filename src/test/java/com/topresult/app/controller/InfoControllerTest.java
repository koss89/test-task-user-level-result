package com.topresult.app.controller;

import com.topresult.app.model.Model;
import com.topresult.app.service.StorageService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.mockito.BDDMockito.*;

@SpringBootTest
@AutoConfigureMockMvc
class InfoControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    StorageService service;

    @Test
    public void addInfo() throws Exception  {
        Model model = new Model(1L, 1L, 22L);
        System.out.println(new ObjectMapper().writeValueAsString(model));
        mockMvc.perform(put("/setinfo")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(model)))
                .andExpect(status().isOk());

        verify(this.service, times(1)).add(any());
    }

    @Test
    public void getByUser() throws Exception {

        List<Model> models = new ArrayList<>();
        models.add(new Model(1L, 1L, 22L));

        given(this.service.getByUserId(1L)).willReturn(models);

        mockMvc.perform(get("/userinfo/1"))
                .andExpect(status().isOk())
                .andExpect(content().json("[{\"result\":22,\"levelId\":1}]"));
    }

    @Test
    public void getByLevel() throws Exception {

        List<Model> models = new ArrayList<>();
        models.add(new Model(1L, 1L, 22L));

        given(this.service.getByLevelId(1L)).willReturn(models);

        mockMvc.perform(get("/levelinfo/1"))
                .andExpect(status().isOk())
                .andExpect(content().json("[{\"result\":22,\"userId\":1}]"));
    }

}
