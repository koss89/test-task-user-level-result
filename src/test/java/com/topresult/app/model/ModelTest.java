package com.topresult.app.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ModelTest {

    @Test
    public void getterSetter()
    {
        Model model = new Model(1L, 1L, 1L);
        Assertions.assertNotNull(model);
        model.setLevelId(2L);
        model.setResult(3L);
        model.setUserId(4L);
        Assertions.assertEquals(2L, model.getLevelId());
        Assertions.assertEquals(3L, model.getResult());
        Assertions.assertEquals(4L, model.getUserId());
    }
}
