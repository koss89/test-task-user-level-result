package com.topresult.app.service;

import com.topresult.app.exception.UserLevelAlreadyExistsException;
import com.topresult.app.model.Model;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
public class StorageServiceImplTest {

    @Autowired
    StorageService service;

    @Test
    public void add() {
        Model model = new Model(1L, 1L, 1L);
        Model secondModel = new Model(2L, 1L, 1L);
        service.add(model);
        service.add(secondModel);
        List<Model> byUserId = service.getByUserId(1L);
        Assertions.assertEquals(model, byUserId.get(0));
    }

    @Test
    public void getByUser() {
        ReflectionTestUtils.setField(service, "collection", new ArrayList<>());
        Model model = new Model(1L, 1L, 1L);
        service.add(model);
        List<Model> byUserId = service.getByUserId(1L);
        Assertions.assertEquals(model, byUserId.get(0));
    }

    @Test
    public void getByLevel() {
        ReflectionTestUtils.setField(service, "collection", new ArrayList<>());
        Model model = new Model(1L, 1L, 1L);
        Model secondModel = new Model(1L, 2L, 1L);
        service.add(model);
        service.add(secondModel);
        List<Model> byUserId = service.getByLevelId(1L);
        Assertions.assertEquals(model, byUserId.get(0));
    }

    @Test
    public void add_failByLevel() {
        ReflectionTestUtils.setField(service, "collection", new ArrayList<>());
        Model model = new Model(1L, 1L, 1L);
        service.add(model);
        Assertions.assertThrows(UserLevelAlreadyExistsException.class, () -> {
            service.add(model);
        });
    }

}
