package com.topresult.app.dto;

import com.topresult.app.dto.LevelInfoDTO;
import com.topresult.app.model.Model;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class LevelInfoDTOTest {

    @Test
    public void create() {
        Model model = new Model(1L, 2L, 3L);
        LevelInfoDTO dto = new LevelInfoDTO(model);
        Assertions.assertNotNull(dto);
        Assertions.assertEquals(3L, dto.getResult());
        Assertions.assertEquals(1L, dto.getUserId());
    }

    @Test
    public void getterSetter()
    {
        Model model = new Model(1L, 2L, 1L);
        LevelInfoDTO dto = new LevelInfoDTO(model);
        dto.setResult(3L);
        dto.setUserId(4L);
        Assertions.assertEquals(3L, dto.getResult());
        Assertions.assertEquals(4L, dto.getUserId());

    }
}
