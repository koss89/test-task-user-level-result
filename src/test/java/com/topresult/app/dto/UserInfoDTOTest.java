package com.topresult.app.dto;

import com.topresult.app.model.Model;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class UserInfoDTOTest {

    @Test
    public void create() {
        Model model = new Model(1L, 2L, 3L);
        UserInfoDTO dto = new UserInfoDTO(model);
        Assertions.assertNotNull(dto);
        Assertions.assertEquals(3L, dto.getResult());
        Assertions.assertEquals(2L, dto.getLevelId());
    }

    @Test
    public void getterSetter()
    {
        Model model = new Model(1L, 2L, 1L);
        UserInfoDTO dto = new UserInfoDTO(model);
        dto.setResult(3L);
        dto.setLevelId(4L);
        Assertions.assertEquals(3L, dto.getResult());
        Assertions.assertEquals(4L, dto.getLevelId());

    }
}
